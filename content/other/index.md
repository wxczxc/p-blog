YCTech Support Terms Of Service {style="font-size:22px"}
======================================

Information we collect
======================

Here is an explanation of the types of information we collect when you
use our services:

-   Personal information: We collect the information you choose to share
    with us when setting up a new account. This type of information
    includes your name, phone number and birthday.
-   Device information: We collect device-specific information, such as
    the hardware model and the version of YCTech Support you are
    using on your phone to provide you better support.
-   Location information: Your location information is stored in our
    server in order to help you share information with your friends and
    optimize your Search experience.
-   Contact information: Your contact book is stored in our server in
    order to optimize your app experience and help you better connect
    with your friends. Your contact book is not published or disclosed
    for any other purpose without your consent.

How we use information
======================

We are passionate about creating engaging and customized experiences for
people. We use all of the information we have to help us provide and
support our Services. Here’s how:

-   Develop, operate, improve, deliver, maintain, and protect our
    products and services.
-   Communicate with you about our Services and let you know about our
    policies and terms. We also use your information to respond to you
    when you contact us.
-   Enhance the safety and security of our products and services.
-   Verify your identity and prevent fraud or other unauthorized or
    illegal activity.
-   Enforce our Terms of Service and other usage policies.

How we share information
========================

We handle your data securely by transmission over https. The information
we collect is not shared to any third parties.

 

* * * * *

 

Điều khoản sử dụng {style="font-size:22px"}
==================

Vui lòng đọc kỹ Thỏa Thuận Sử Dụng (“Thỏa Thuận”) trước khi bạn tiến
hành tải, cài đặt, sử dụng tất cả hoặc bất kỳ phần nào của ứng dụng
“YCTech Support” (“Ứng Dụng”) (bao gồm nhưng không giới hạn phần
mềm, các file và các tài liệu liên quan) hoặc sử dụng các dịch vụ do
Công Ty Cổ Phần TL (“TL”) cung cấp để kết nối đến Ứng Dụng. Bạn chấp
thuận và đồng ý bị ràng buộc bởi các quy định và điều kiện trong Thỏa
Thuận này khi thực hiện các thao tác trên đây. Trường hợp bạn không đồng
ý với bất kỳ điều khoản sử dụng nào của chúng tôi (phiên bản này và các
phiên bản cập nhật), bạn vui lòng không tải, cài đặt, sử dụng Ứng dụng
hoặc tháo gỡ Ứng Dụng ra khỏi thiết bị di động của bạn.

 

1. Cập nhật:
============

Thỏa Thuận này có thể được cập nhật thường xuyên bởi TL. Phiên bản cập
nhật sẽ thay thế cho các quy định và điều kiện trong thỏa thuận ban đầu.
Bạn có thể truy cập vào Ứng Dụng hoặc vào website trên đây để xem nội
dung chi tiết của phiên bản cập nhật.

 

2. Giới Thiệu Về Ứng Dụng
=========================

YCTech Support là ứng dụng nhắn tin và kết nối đa phương tiện
dành riêng cho người dùng di động tại Việt Nam. Ứng dụng sử dụng số điện
thoại và danh bạ làm nền tảng với các tính năng chính: (1) Nhắn tin bằng
giọng nói, (2) Kết nối theo sở thích và địa điểm, (3) Chia sẻ khoảnh
khắc và cảm xúc. Ứng dụng hỗ trợ tất cả các nền tảng Android, IOS và
Java.

 

3. Quyền Sở Hữu Ứng Dụng
========================

Ứng Dụng này được phát triển và sở hữu bởi TL, tất cả các quyền sở hữu
trí tuệ liên quan đến Ứng Dụng (bao gồm nhưng không giới hạn mã nguồn,
hình ảnh, dữ liệu, thông tin, nội dung chứa đựng trong Ứng Dụng; các sửa
đổi, bổ sung, cập nhật của Ứng Dụng) và các tài liệu hướng dẫn liên quan
(nếu có) sẽ thuộc quyền sở hữu duy nhất bởi TL và không cá nhân, tổ chức
nào được phép sao chép, tái tạo, phân phối, hoặc hình thức khác xâm phạm
tới quyền của chủ sở hữu nếu không có sự đồng ý và cho phép bằng văn bản
của TL.

 

4. Tài Khoản
============

Để sử dụng Ứng Dụng bạn phải tạo một tài khoản theo yêu cầu của chúng
tôi, bạn cam kết rằng việc sử dụng tài khoản phải tuân thủ các quy định
của TL, đồng thời tất cả các thông tin bạn cung cấp cho chúng tôi là
đúng, chính xác, đầy đủ với tại thời điểm được yêu cầu. Mọi quyền lợi và
nghĩa vụ của bạn sẽ căn cứ trên thông tin tài khoản bạn đã đăng ký, do
đó nếu có bất kỳ thông tin sai lệch nào chúng tôi sẽ không chịu trách
nhiệm trong trường hợp thông tin đó làm ảnh hưởng hoặc hạn chế quyền lợi
của bạn.

 

5. Tài Khoản
============

Bạn có quyền sử dụng Ứng Dụng và các dịch vụ khác mà chúng tôi cung cấp,
tuy nhiên việc sử dụng đó sẽ không bao gồm các hành vi sau đây nếu không
có sự đồng ý bằng văn bản của TL.

-   Sao chép, chỉnh sửa, tái tạo, tạo ra sản phẩm mới hoặc phiên bản
    phái sinh trên cơ sở Ứng Dụng này;
-   Bán, chuyển giao, cấp quyền lại, tiết lộ hoặc hình thức chuyển giao
    khác hoặc đưa một phần hoặc toàn bộ Ứng Dụng cho bất kỳ bên thứ ba;
-   Sử dụng Ứng Dụng để cung cấp dịch vụ cho bất kỳ bên thứ ba (tổ chức,
    cá nhân);
-   Di chuyển, xóa bỏ, thay đổi bất kỳ thông báo chính đáng hoặc dấu
    hiệu nào của Ứng Dụng (bao gồm nhưng không giới hạn các tuyên bố về
    bản quyền);
-   Thiết kế lại, biên dịch, tháo gỡ, chỉnh sửa, đảo lộn thiết kế của
    Ứng Dụng hoặc nội dung Ứng Dụng;
-   Thay đổi hoặc hủy bỏ trạng thái ban đầu của Ứng Dụng;
-   Sử dụng Ứng Dụng để thực hiện bất kỳ hành động gây hại cho hệ thống
    an ninh mạng của TL, bao gồm nhưng không giới hạn sử dụng dữ liệu
    hoặc truy cập vào máy chủ hệ thống hoặc tài khoản không được phép;
    truy cập vào hệ thống mạng để xóa bỏ, chỉnh sửa và thêm các thông
    tin; phát tán các chương trình độc hại, virus hoặc thực hiện bất kỳ
    hành động nào khác nhằm gây hại hoặc phá hủy hệ thống mạng;
-   Đăng nhập và sử dụng Ứng Dụng bằng một phần mềm tương thích của bên
    thứ ba hoặc hệ thống không được phát triển, cấp quyền hoặc chấp
    thuận bởi TL;
-   Sử dụng, bán, cho mượn, sao chép, chỉnh sửa, kết nối tới, phiên
    dịch, phát hành, công bố các thông tin liên quan đến Ứng Dụng, xây
    dựng mirror website để công bố các thông tin này hoặc để phát triển
    các sản phẩm phái sinh, công việc hoặc dịch vụ;
-   Sử dụng Ứng Dụng để đăng tải, chuyển, truyền hoặc lưu trữ các thông
    tin vi phạm pháp luật, vi phạm thuần phong mỹ tục của dân tộc;
-   Sử dụng Ứng Dụng để sử dụng, đăng tải, chuyển, truyền hoặc lưu trữ
    bất kỳ nội dung vi phạm quyền sở hữu trí tuệ, bí mật kinh doanh hoặc
    quyền pháp lý của bên thứ ba;
-   Sử dụng Ứng Dụng hoặc các dịch vụ khác được cung cấp bởi TL trong
    bất kỳ hình thức vi phạm pháp luật nào, cho bất kỳ mục đích bất hợp
    pháp nào;
-   Các hình thức vi phạm khác.

 

6. Xử Lý Vi Phạm
================

Trường hợp bạn vi phạm bất kỳ quy định nào trong Thỏa Thuận này, TL có
quyền ngay lập tức khóa tài khoản của bạn và/hoặc xóa bỏ toàn bộ các
thông tin, nội dung vi phạm, đồng thời tùy thuộc vào tính chất, mức độ
vi phạm bạn sẽ phải chịu trách nhiệm trước cơ quan có thẩm quyền, TL và
bên thứ ba về mọi thiệt hại gây ra bởi hoặc xuất phát từ hành vi vi phạm
của bạn.

 

7. Quyền Truy Cập và Thu Thập Thông Tin
=======================================

​(a) Khi sử dụng Ứng Dụng, bạn thừa nhận rằng chúng tôi có quyền sử dụng
những API hệ thống sau để truy cập vào dữ liệu trên điện thoại của bạn:
(1) Đọc và ghi vào danh bạ điện thoại, (2) Lấy vị trí hiện tại của bạn
khi được sự đồng ý, (3) Ghi dữ liệu của Ứng Dụng lên thẻ nhớ, (4) Truy
cập vào Internet từ thiết bị của bạn. Tất cả các truy cập này đều được
chúng tôi thực hiện sau khi có sự đồng ý của bạn, vì vậy bạn cam kết và
thừa nhận rằng, khi bạn đã cấp quyền cho chúng tôi, bạn sẽ không có bất
kỳ khiếu nại nào đối với TL về việc truy cập này.

​(b) Cùng với quyền truy cập, chúng tôi sẽ thu thập các thông tin sau
của bạn

-   Thông tin cá nhân: bao gồm các thông tin bạn cung cấp cho chúng tôi
    để xác nhận tài khoản như tên, số điện thoại, số chứng minh nhân
    dân, địa chỉ email;
-   Thông tin chung: như các thông tin về cấu hình điện thoại của bạn,
    thông tin phiên bản YCTech Support mà bạn đang sử dụng cho
    điện thoại của mình;
-   Thông tin vị trí của bạn: dữ liệu về vị trí địa lý của bạn sẽ được
    lưu trữ trên máy chủnhằm giúp bạn sử dụng chức năng tìm kiếm của Ứng
    Dụng;
-   Danh bạ điện thoại: chúng tôi sẽ lưu trữ danh bạ điện thoại của bạn
    trên máy chủ nhằm hỗ trợ tốt nhất cho bạn trong việc sử dụng Ứng
    Dụng và tránh trường hợp bạn bị mất dữ liệu. Chúng tôi cam kết sẽ
    tôn trọng và không sử dụng danh bạ điện thoại của bạn vì bất kỳ mục
    đích nào nếu không có sự đồng ý của bạn;
-   Chúng tôi không sử dụng bất kỳ biện pháp nào để theo dõi nội dung
    tin nhắn, trao đổi hoặc hình thức khác nhằm theo dõi người dùng khi
    sử dụng Ứng Dụng này.

 

8. Cam Kết Bảo Mật Thông Tin
============================

TL sử dụng các phương thức truyền tin an toàn https và mã hóa để truyền
tải và lưu trữ các dữ liệu cá nhân và giao tiếp của bạn. Chúng tôi cam
kết giữ bí mật tất cả thông tin mà bạn cung cấp cho TL hoặc chúng tôi
thu thập từ bạn và không tiết lộ với bất kỳ bên thứ ba nào trừ khi có
yêu cầu từ Cơ quan Nhà nước có thẩm quyền.

 

9. Phí Và Các Khoản Thu
=======================

TL cam kết không thu bất cứ khoản phí nào từ người dùng cho các dịch vụ
cơ bản mà hiện tại chúng tôi đang cung cấp.

 

10. Quảng cáo và các nội dung thương mại được phân phối bởi YCTech Support
=================================================================================

Khi sử dụng ứng dụng, bạn thừa nhận rằng chúng tôi có quyền sử dụng các
thông tin không định danh của bạn nhằm cung cấp các nội dung quảng cáo
đúng đối tượng.

 

11. Lưu Ý Sử Dụng
=================

Một số tính năng kết bạn theo sở thích hay địa điểm như: Tìm bạn quanh
đây có thể gây phiền toái cho người sử dụng. Khi sử dụng tính năng này,
bạn có thể tìm thấy được bạn mới và đồng thời bạn cũng sẽ được tìm thấy
bởi người lạ.Bạn phải cẩn thận khi quyết định hẹn gặp một người lạ thông
qua tính năng này, nếu bạn phát hiện người lạ có dấu hiệu lừa đảo hoặc
phạm tội, xin hãy báo cáo lại cho chúng tôi hoặc cơ quan Pháp Luật gần
nhất.Nếu bạn cảm thấy phiền toái sau khi sử dụng tính năng này, hãy chọn
Ẩn vị trí để không bị làm phiền.

 

12. Liên Lạc Với Chúng Tôi
==========================

- Địa chỉ email
[hit.gamevn@gmail.com](hit.gamevn@gmail.com);
hoặc

 

**Trân trọng cảm ơn bạn đã sử dụng sản phẩm và dịch vụ của chúng tôi.**
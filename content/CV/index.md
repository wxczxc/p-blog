# PHAM VIET LONG
{{< rawhtml >}}
<table>
  <tr>
    <td><b>Phone</b></td>
    <td>09 04.915.863</td>
    <td rowspan="4">
        <img src="https://wxczxc.gitlab.io/p-blog/cai_met.png" />
    </td>
  </tr>
  <tr>
    <td><b>Email</b></td>
    <td><a href="mailto:sior910@gmail.com">sior910@gmail.com</a></td>
  </tr>
  <tr>
    <td><b>Birthdate</b></td>
    <td>09/10/1988</td>
  </tr>
  <tr>
    <td><b>Address</b></td>
    <td>No. 1210, OCT2-DN3, 79 Dang Xuan Bang, Linh Dam, Ha Noi</td>
  </tr>
</table>
{{< /rawhtml >}}

# Personal Summary 
> With more than 7 years working in IT industry in different positions, included 2 years startup and working as a fullstack developer in my own company, I have lot experience in backend field, working with big and high scalability systems, come with many flavor of OTT system, real time messaging, social networking and payment solution. I can working under pleasure and flexible with many roles in a project ( from design / analytics database to writing side tools for CI/CD , automation … ) I enjoy learning more about new technology and improving my skills, and work under mid to enterprises level. 
# Education 
- **Hanoi Aptech (2010 – 2013)** <br/>
*Higher Diploma in Software Engineering*
    * Software Engineering with specific target 
    * Basic Software Development 
- **FPT Greenwich (2013 – 2014)** <br/>
*quit after semester 2 due to personal finance issues* 
*The Degree of Engineer in Information Technology*
    * Software Engineering 
    * Software Production 
    * Networking
    * Discrete Mathematics / Probability and Mathematical Statistics 
    
# Key Knowledges 
- More than 7 years working in IT industry, familiar with Agile / Scrum , Waterfall or TDD / TBD progress. 
- Familiar with popular database engine ( MySQL, PostgreSQL, Redis, Mongo, CouchDB... )
- Good experience as backend developer ( PHP , Goland, Java, C# ) ● Experiment with microservices architect, Docker, AWS 
- Having experience in Networking, Socket, Thread processing… 
- Familiar with 3 OS systems ( Windows, OSX, *nix) 
- Good experience in development and publishing mobile games / app on iOS, Android. 
- English skill is good enough for communication and documentation 
    
# Employment History 
## VinID - 1MG Group (Jun 2019 - present)
- Position: Backend developer / Technical Owner 
- Task Summary: 
    - Working as backend developer 
- Featured projects: 
    > *Currently un-available due to NDA*
    - **Ref:** 
        - https://developers.vinid.net/
        - https://play.google.com/store/apps/details?id=com.vingroup.oneseal&hl=en_US&gl=US
        - https://vinshop.vn/
    - **Tech-stacks:** 
        - Golang / Java for backend 
        - Mysql / Redis / Elastic
        - Google Cloud Platform 
        - Flutter
        - Kafka / Kong ...
## Fujitech ., JSC (October 2017 – Jun 2019) 
- Position: Full-stack developer / Leader 
- Task Summary: 
    - Working as full-stack developer in both web / mobile projects 
    - Interview with applied candidates for technical positions 
    - Develop new features / maintain in-house payment system for mobile games 
    - Training intern developers - Manager / Reporter / Maintainer on company’s infrastructure and resources ( mainly AWS ) 
- Featured projects: 
    1. Murakawa Management Systems <br/>
        > *In-house management system for a Japanese heavy industry company. Using web and mobile apps, employees in company can register / vote / share their knowledge / video and chat with another members. Also support company manager to manage all employees timekeeping, salary invoice and bonus.* <br/>
    - **Ref:** 
        - https://murakawa.xyz 
        - https://play.google.com/store/apps/details?id=com.fujitechjsc.murakawa <br/>
    - **Role assigned:** 
        - Full-stack developer / leader 
        - Design architect, database and develop backend 
    - **Tech-stacks:** 
        - Headless Wordpress / Golang for backend 
        - Mysql, 
        - Node.js ( socket.io ) for chat systems 
        - React native for mobile apps 
    2. Yume100 
        > *Co-Publish Japanese mobile game in Vietnam.* 
    - **Ref:**
        - http://yume100.vn/ 
        - https://fujigame.vn/ 
    - **Role assigned:** 
        - Game producer. 
        - Backend developer 
    - **Tech-stacks:**    
        - Laravel for landing page, KPI tools 
        - Java ( Play framework ) for payment API 
    3. Data Artist website 
        > *Develop website and partly Data Progressing Module for an AI company from scratch*
    - **Ref:**    
        - https://www.data-artist.com/en/ 
    - **Role assigned:** 
        - Backend developer 
    - **Tech-stacks:**    
        - Python for convert raw data ( csv, excels, text files .. ) into database 
        - Design database schema, optimize performance of whole system 
        - PHP for both backend and frontend. 
## YCTech ., JSC (January 2016 – October 2017) 
- Position: Co-Founder / CEO / Full-stack developer 
- Task Summary: 
    - Working as full-stack developer 
    - Building outsource projects and our own products 
    - Doing paperwork. 
- Featured projects: 
    1. YOJEE System 
    > *An outsourcing project for logistics companies based in Australia, similar like Uber for X’s systems.* 
    - **Ref:**    
        - https://yojee.com/ 
    - **Role assigned:** 
        - Develop management web using Ruby on Rails. 
        - Develop customer app using react-native. 
        - Maintain / Develop / Optimize internal websites. 
    - **Tech-stacks:**
        - Ruby on Rails for management site. 
        - HTML / vanilla JS for static sites. 
        - React-native 
    2. iHOTELVN 
    > *An SAAS systems for hotel management come in web, desktop and mobile platforms* 
    - **Role assigned:** 
        - Design and analytics with system architect 
        - Coding ( mainly on frontend ) using PHP 
        - Coding desktop app 
        - Research / planning for marketing, co-op with partners 
    - **Tech-stacks:** 
        - Golang for backend 
        - PostgreSQL, Redis - PHP ( FuelPHP ) for frontend, landing page 
        - NW.js for desktop app 
## Egroup / Apax English (November 2015 – January 2016) 
- Position: Senior developer 
- Task summary: 
    - Coding backend ( mainly in PHP ) for in-house management system and game portal. 
## Sgame / Clamp Studio (June 2014 – Oct 2015) 
- Position: Developer 
- Task summary: 
    - Coding backend / web crawler / KPI / Data summary tools using PHP and C# 
    - Develop mobile games using Unity3D ( C# ) 
## Peanut Game ., JSC (Oct 2013 – December 2014) 
- Position: Game Developer 
- Task summary: 
    - Develop mobile games using Unity3D ( C# ) and Cocos2D-x 
## Gameloft (Jul 2011 – April 2013) 
- Position: Game Designer / Tester 
- Task summary: 
    - Test mobile games 
    - Design / partly coding game UI using Action Script ( Flash ) 
# Other skills / Tools 
- Basic Photoshop / 3D-Max 
- Good at Excels / Powerpoint 
# Languages 
- English 
# Achievements 
** 1st Windows Phone Best Game Challenger 2014 (Microsoft’s event) **\
[https://vnexpress.net/bombi-saga-danh-chien-thang-ty-phu-ung-dung-windows-phone-3108949.html](https://vnexpress.net/bombi-saga-danh-chien-thang-ty-phu-ung-dung-windows-phone-3108949.html)

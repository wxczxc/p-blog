### Long Pham Viet
*Digital Craftsman ( "Somewhat an" Artist / Developer / Father )*
Vietnam

## Work
I'm a freelance and a full-stack developer.

## Bio
1988 Born in Ha Noi, Vietnam.

2010 Work as developer.

Present *keep breathing*

## I ♥
Art, Music, Drawing

Got some works to request to me?

That's great! I'm available for freelance work, I want to hear about your projects. Please submit your message from the contact form.

## Contact me

[E-mail](mailto:sior910@gmail.com)